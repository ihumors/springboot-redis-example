# Jedis,Redisson,Lettuce 介绍说明

### 共同点：

都提供了基于 Redis 操作的 Java API，只是封装程度，具体实现稍有不同。

### 不同点：

#### 1、Jedis

是 Redis 的 Java 实现的客户端。支持基本的数据类型如：String、Hash、List、Set、Sorted Set。

特点：使用阻塞的 I/O，方法调用同步，程序流需要等到 socket 处理完 I/O 才能执行，不支持异步操作。Jedis 客户端实例不是线程安全的，需要通过连接池来使用 Jedis。

#### 2、Redisson

优点：分布式锁，分布式集合，可通过 Redis 支持延迟队列。

#### 3、Lettuce

用于线程安全同步，异步和响应使用，支持集群，Sentinel，管道和编码器。

基于 Netty 框架的事件驱动的通信层，其方法调用是异步的。Lettuce 的 API 是线程安全的，所以可以操作单个 Lettuce 连接来完成各种操作。

参考文章：https://blog.csdn.net/qq_42105629/article/details/102589319