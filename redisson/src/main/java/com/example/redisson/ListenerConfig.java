package com.example.redisson;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.api.listener.MessageListener;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class ListenerConfig {

    @Bean
    CommandLineRunner listenerRedissonPublish(RedissonClient redissonClient) {
        return args -> {
            final RTopic rTopic = redissonClient.getTopic("example:redis:redisson:topic:my01");
            rTopic.addListener(String.class, new MessageListener<String>() {
                @Override
                public void onMessage(CharSequence charSequence, String s) {
                    log.info("收到主题:\"example:redis:redisson:topic:my01\" 的消息内容：{}", s);
                }
            });
        };
    }
}
