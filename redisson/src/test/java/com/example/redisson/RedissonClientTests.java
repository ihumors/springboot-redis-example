package com.example.redisson;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.*;

import javax.annotation.Resource;

@Slf4j
public class RedissonClientTests extends WebApplicationTests {
    @Resource
    private RedissonClient redissonClient;

    @Test
    public void getBucketTest() {
        RBucket<String> rBucket = redissonClient.getBucket("example:redis:redisson");
        rBucket.set("redisson test value");
        log.info("通过key获取value", rBucket.get());
    }

    @Test
    public void setTest() {
        RScoredSortedSet<String> rSet = redissonClient.getScoredSortedSet("ScoredSortedSet");

        rSet.add(0.33, "孔子-性善论");
        rSet.add(0.251, "孟子-老师（孔子孙子子思）");
        rSet.add(0.302, "荀子-性恶论");
        rSet.add(0.5, "韩非");
        rSet.add(1, "秦始皇宰相李斯");
        rSet.add(1.2, "lihm");

        log.info("所有元素列表信息:{}", rSet.readAll());
        log.info("获取元素”lihm“在集合中的位置为：{}", rSet.rank("lihm"));
        log.info("获取元素“lihm”的评分为：{}", rSet.getScore("lihm"));
    }

    @Test
    public void queueTest() {
        RQueue<String> rQueue = redissonClient.getQueue("example:redis:redisson:queue");
        rQueue.add("孔子");
        rQueue.add("孟子");
        rQueue.add("荀子");
        log.info("queue list:{}", rQueue.readAll());
    }

    @Test
    public void publishTest() {
        RTopic rTopic = redissonClient.getTopic("example:redis:redisson:topic:my01");
        final long receivedClientNums = rTopic.publish("邓鹏");
        log.info("收到客户端的数量:{}", receivedClientNums);
    }
}
