## redisson配置玩法

### 第一种使用公用的spring-boot-data-redis配置

```python
spring:
  redis:
    #redis数据库
    database: 9
    #redis服务地址
    host: 192.168.0.187
    #redis服务端口
    port: 6379
    #redis服务认证密码，单机密码为空，集群密码为123456
    password: 123456
    #超时时间
    timeout: 10000ms
    cluster:
      max-redirects: 2
      nodes: 192.168.0.188:7001,192.168.0.188:7002,192.168.0.188:7003,192.168.0.188:7004,192.168.0.188:7005,192.168.0.188:7006
    
```

### 第二种使用redisson的配置

单机配置

```python
spring:
  redis:
    redisson:
      # 单机模式
      config: |
        singleServerConfig:
          idleConnectionTimeout: 10000
          connectTimeout: 10000
          timeout: 10000
          retryInterval: 10000
          retryAttempts: 3
          subscriptionsPerConnection: 5
          clientName: null
          password:
          address: "redis://192.168.0.187:6379"
          database: 9
          subscriptionConnectionMinimumIdleSize: 1
          subscriptionConnectionPoolSize: 25
          connectionMinimumIdleSize: 5
          connectionPoolSize: 100
          dnsMonitoringInterval: 5000
        threads: 16
        nettyThreads: 32
        codec: !<org.redisson.codec.MarshallingCodec> {}
        transportMode: "NIO"
```

集群配置

```python
spring:
  redis:
    #redis数据库
    database: 9
    redisson:
     # 集群模式
      config: |
        clusterServersConfig:
          idleConnectionTimeout: 10000
          connectTimeout: 10000
          timeout: 3000
          retryAttempts: 3
          retryInterval: 1500
          failedSlaveReconnectionInterval: 3000
          failedSlaveCheckInterval: 60000
          password: 123456
          subscriptionsPerConnection: 5
          clientName: example
          loadBalancer: !<org.redisson.connection.balancer.RoundRobinLoadBalancer> {}
          subscriptionConnectionMinimumIdleSize: 1
          subscriptionConnectionPoolSize: 50
          slaveConnectionMinimumIdleSize: 24
          slaveConnectionPoolSize: 64
          masterConnectionMinimumIdleSize: 24
          masterConnectionPoolSize: 64
          readMode: "SLAVE"
          subscriptionMode: "SLAVE"
          nodeAddresses:
          - "redis://192.168.0.188:7001"
          - "redis://192.168.0.188:7002"
          - "redis://192.168.0.188:7003"
          - "redis://192.168.0.188:7004"
          - "redis://192.168.0.188:7005"
          - "redis://192.168.0.188:7006"
          scanInterval: 1000
          pingConnectionInterval: 0
          keepAlive: false
          tcpNoDelay: false
        threads: 16
        nettyThreads: 32
        codec: !<org.redisson.codec.MarshallingCodec> {}
        transportMode: "NIO"
```

更多模式配置信息查阅：https://github.com/redisson/redisson/wiki/2.-Configuration

github源码地址：https://github.com/redisson/redisson/tree/master/redisson-spring-boot-starter#spring-boot-starter