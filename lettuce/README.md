### spring-boot-starter-data-redis

默认使用的就是lettuce连接池

连接池的配置如下

```python
spring:
  redis:
    #redis服务地址
    host: 192.168.0.187
    #redis服务端口
    port: 6379
    #redis服务认证密码，单机密码为空，集群密码为123456
    password: 123456
    #redis数据库
    database: 4
    #超时时间
    timeout: 10000ms
    #redis连接池管理
    lettuce:
      pool:
        #最大连接数 默认为8
        max-active: 1024
        #最小空闲连接 默认0
        max-wait: 10000ms
        #最大空闲连接 默认8
        max-idle: 200
        #最大连接阻塞等待时间
        min-idle: 5
    cluster:
      max-redirects: 2
      nodes: 192.168.0.188:7001,192.168.0.188:7002,192.168.0.188:7003,192.168.0.188:7004,192.168.0.188:7005,192.168.0.188:7006
  
```