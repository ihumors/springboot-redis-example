package com.example.lettuce;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * opsForHash --> hash操作
 * opsForList --> list操作
 * opsForSet --> set操作
 * opsForValue --> string操作
 * opsForZSet --> Zset操作
 */
@Slf4j
public class RedisTemplateTests extends WebApplicationTests {
    private static final List<String> LIST_STR = Arrays.asList("lettuce1", "lettuce2", "lettuce3");
    private static final String KEY = "example:redis:lettuce";


    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void opsForHashTest() {
        final String hashKey = "opsForHash";
        redisTemplate.opsForHash().put(KEY, hashKey, LIST_STR);
        log.info("key:{},hashKey:{},key:{}", KEY, hashKey, redisTemplate.opsForHash().get(KEY, hashKey));
    }

    @Test
    public void opsForListTest() {
        final String hashKey = "opsForList";
        redisTemplate.opsForList().rightPush(KEY+":"+hashKey,LIST_STR);
        log.info("key:{}:{},key:{}", KEY, hashKey, redisTemplate.opsForList().rightPop(KEY+":"+hashKey, 2));
    }
}
